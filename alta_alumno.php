<?php
include 'conexion.php';

// Variables
$image = $_POST ['image'];
$name= $_POST ['nombre'];
$lastname= $_POST ['lastname'];
$sex = $_POST ['sexo'];
$nacimiento = $_POST['birthdate'];
$mail = $_POST['email'];
$phone = $_POST['phone'];
$direction = $_POST['direction'];
$province = $_POST['province'];
$country = $_POST['country'];
$modlle_user = $_POST['username'];
$modlle_pass= $_POST['password'];
$cicle = $_POST['cicle'];
$modulo = $_POST['modulo'];
$prefe = $_POST['prefe'];
$linkedin = $_POST['linkedin'];
$ingles = $_POST['ingles'];

// Instruccion SQL
$sql = "INSERT INTO usuario
    (
        imagen,
        nombre,
        apellido,
        sexo,
        nacimiento,
        mail,
        telefono,
        direccion,
        provincia,
        pais_nacimiento,
        usuario,
        pass,
        ciclo,
        modulo,
        preferencia,
        linkedin,
        ingles
    )
    VALUES
    (
        '$image',
        '$name',
        '$lastname',
        '$sex',
        '$nacimiento',
        '$mail',
        '$phone',
        '$direction',
        '$province',
        '$country',
        '$modlle_user',
        '$modlle_pass',
        '$cicle',
        '$modulo',
        '$prefe',
        '$linkedin',
        '$ingles'
    );";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="author" content="Dragos C. Butnariu">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="./style.css">
  <link rel="shotycut icon" type="image/png" href="./media/user.png">
  <meta charset="UTF-8">
  <title>Dando de alta...</title>
</head>
<body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-sale">
          <a class="navbar-brand" href="#">
              Salesianos Domingo Savio
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    Insertar
                    <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="consulta.php">
                    Total
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="filtrar.html">
                    Filtrar
                </a>
              </li>
            </ul>
          </div>
        </nav>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1 class="text-center">
<?php
if ( mysqli_query($conn, $sql)) { echo 'Se ha dado de alta correctamente a ' , $name , ' ', $lastname; }
?>
        </h1>
      </div>
      <div class="col-12 text-center">
        <a href="index.html">
          <button class="btn btn-sale">
            Insertar Nuevo
          </button>
        </a>
        <a href="consulta.php">
          <button class="btn btn-sale">
            Total Alumnos
          </button>
        </a>
      </div>
    </div>
  </div>
</body>
</html>
