<?php
include 'conexion.php';

// Variables
$name= $_POST ['nombre'];
$apell = $_POST ['lastname'];
$sex = $_POST ['sexo'];
$mail = $_POST['email'];
$phone = $_POST['phone'];
$direction = $_POST['direction'];
$province = $_POST['province'];
$country = $_POST['country'];
$cicle = $_POST['ciclo'];
$modulo = $_POST['modulo'];
$prefe = $_POST['prefe'];
$ingles = $_POST['ingles'];

// Consulta
$sql = "SELECT id, nombre, apellido, sexo, nacimiento, mail, telefono, direccion, provincia, pais_nacimiento, usuario, ciclo, modulo, preferencia, linkedin, ingles
    FROM $dbname.usuario
    WHERE
    nombre = '$name' OR
    apellido = '$apell' OR
    sexo = '$sex' OR
    mail = '$mail' OR
    direccion = '$direction' OR
    provincia = '$province' OR
    pais_nacimiento = '$country' OR
    ciclo = '$cicle' OR
    modulo = '$modulo' OR
    preferencia = '$prefe' OR
    ingles = '$ingles'
";

$result = mysqli_query($conn,$sql);
?>
<!DOCTYPE html>
<html lang="es-ES">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="author" content="Dragos C. Butnariu">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="./style.css">
        <link rel="shotycut icon" type="image/png" href="./media/user.png">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="./script.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script>
$(document).ready(function () {
    $('#alumnos').DataTable( {
    "searching": false,
        "ordering": false,
        "pageLength": 5,
        "lengthMenu": [ 5, 15, 50, 100, 250 ],
        "language":
{
    "paginate": {
    "next": "Siguiente",
        "previous": "Anterior"
},
    "info": " Página _PAGE_ de _PAGES_",
    "lengthMenu": "Mostrar _MENU_ entradas"
}
});
$('.dataTables_length').addClass('bs-select');
});
</script>
        <meta charset="UTF-8">
        <title>Consulta Alumnos</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-sale">
          <a class="navbar-brand" href="index.html">
              Salesianos Domingo Savio
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.html">
                    Insertar
                    <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="consulta.php">
                    Total
                </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="filtrar.html">
                    Filtrar
                </a>
              </li>
            </ul>
          </div>
        </nav>
        <div class="jumbotron jumbotron-fluid text-center text-dark">
            <h2 class="display-3">
                Alumno/s Filtrado
            </h2>
            <hr class="my-2"/>
            <p class="lead text-center">
                Aqui se muestra el total de alumnos aplicando el filtro de busqueda
            </p>
        </div>
        <h3 class="text-center">Total alumnos: <?php echo mysqli_num_rows($result); ?> </h3>
        <table id="alumnos" class="table table-bordered table-hover table-responsive table-sm">
          <thead class="bg-sale text-light">
            <tr>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Sexo</th>
              <th>Fecha Nacimiento</th>
              <th>E-Mail</th>
              <th>Telefono</th>
              <th>Direccion</th>
              <th>Provincia</th>
              <th>Pais Nacimiento</th>
              <th>Usuario Modlle</th>
              <th>Ciclo</th>
              <th>Modulo</th>
              <th>Preferencia</th>
              <th>Linkedin</th>
              <th>Nivel Inglés</th>
              <th>Eliminar</th>
            </tr>
          </thead>
<?php
while ( $show = mysqli_fetch_array($result)) {
?>
                 <tr>
                     <th> <?php echo $show['nombre']  ?> </th>
                     <th> <?php echo $show['apellido']  ?> </th>
                     <th> <?php echo $show['sexo']  ?> </th>
                     <th> <?php echo $show['nacimiento']  ?> </th>
                     <th> <?php echo $show['mail']  ?> </th>
                     <th> <?php echo $show['telefono']  ?> </th>
                     <th> <?php echo $show['direccion']  ?> </th>
                     <th> <?php echo $show['provincia']  ?> </th>
                     <th> <?php echo $show['pais_nacimiento']  ?> </th>
                     <th> <?php echo $show['usuario']  ?> </th>
                     <th> <?php echo $show['ciclo']  ?> </th>
                     <th> <?php echo $show['modulo']  ?> </th>
                     <th> <?php echo $show['preferencia']  ?> </th>
                     <th> <?php echo $show['linkedin']  ?> </th>
                     <th> <?php echo $show['ingles']  ?> </th>
                     <th>
                        <a href="elimina_alumno.php?ida=<?php echo $show['id']; ?>">Eliminar</a>
                     </th>
                 </tr>
<?php
}
?>
        </table>
        <footer>
            <br/><br/><br/>
        </footer>
    </body>
</html>
